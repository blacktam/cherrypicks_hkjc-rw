#!/bin/sh

/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-webservice-request.log.*' -mtime +31 -exec rm -f {} \;
/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-webservice-all.log.*' -mtime +31 -exec rm -f {} \;
/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-webservice-error.log.*' -mtime +31 -exec rm -f {} \;

/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-miniweb-request.log.*' -mtime +31 -exec rm -f {} \;
/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-miniweb-all.log.*' -mtime +31 -exec rm -f {} \;
/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-miniweb-error.log.*' -mtime +31 -exec rm -f {} \;

/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-cms-all.log.*' -mtime +31 -exec rm -f {} \;
/usr/bin/find /home/ec2-user/log/tomcat/hkjc  -name 'hkjc-cms-error.log.*' -mtime +31 -exec rm -f {} \;

/usr/bin/find /home/ec2-user/apache-tomcat-7.0.21/logs -type f -mtime +31 -exec rm -f {} \;
