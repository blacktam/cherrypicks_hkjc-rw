#!/bin/sh
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

instanceid=`/opt/aws/bin/ec2-metadata --instance-id | awk '{print $2}'`
instancename=`/usr/bin/aws ec2 describe-tags --filters "Name=resource-type,Values=instance" "Name=resource-id,Values=$instanceid" --region ap-southeast-1 --output text | grep Name | cut -f5`

year=`date +"%Y"`
month=`date +"%m"`
day=`date +"%d"`

if [ -d /media/happywedshirts/public ]
then
	/usr/bin/aws s3 sync /media/happywedshirts/public/ s3://hkjc-rw-prod-log/cms/happywedshirts/data/public/ --region ap-northeast-1 > /tmp/sync_data_log.$year$month$day 2>&1
	if [ -f /tmp/sync_data_log.$year$month$day ]
	then
		/bin/gzip -c /tmp/sync_data_log.$year$month$day > /tmp/sync_data_log.$year$month$day.gz
		/usr/bin/aws s3 cp /tmp/sync_data_log.$year$month$day.gz s3://hkjc-rw-prod-log/cms/happywedshirts/sync_log/year=$year/month=$month/day=$day/ --region ap-northeast-1 > /dev/null 2>&1
		rm -f /tmp/sync_data_log.$year$month$day
		rm -f /tmp/sync_data_log.$year$month$day.gz
	fi
fi
