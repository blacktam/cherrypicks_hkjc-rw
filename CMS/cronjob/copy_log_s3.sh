#!/bin/sh
if [ $# -le 2 ]
then
	echo "Please provide service name, source file path and destination filename"
	echo $#
	exit 1
fi 

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

instanceid=`/opt/aws/bin/ec2-metadata --instance-id | awk '{print $2}'`
instancename=`/usr/bin/aws ec2 describe-tags --filters "Name=resource-type,Values=instance" "Name=resource-id,Values=$instanceid" --region ap-southeast-1 --output text | grep Name | cut -f5`

sourcelog=$2
destinationlogname=$3
servicename=$1
year=`date -d "yesterday" +"%Y"`
month=`date -d "yesterday" +"%m"`
day=`date -d "yesterday" +"%d"`

if [ -f $sourcelog.$year$month$day ]
then
	/bin/gzip -c $sourcelog.$year$month$day > /tmp/$destinationlogname
fi

if [ -f /tmp/$destinationlogname ]
then 
	/usr/bin/aws s3 cp /tmp/$destinationlogname s3://hkjc-rw-prod-log/cms/server_log/year=$year/month=$month/day=$day/$instancename/$servicename/$destinationlogname --region ap-northeast-1 > /dev/null 2>&1
	rm -f /tmp/$destinationlogname
fi
