#!/bin/sh

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

instanceid=`/opt/aws/bin/ec2-metadata --instance-id | awk '{print $2}'`
instancename=`/usr/bin/aws ec2 describe-tags --filters "Name=resource-type,Values=instance" "Name=resource-id,Values=$instanceid" --region ap-southeast-1 --output text | grep Name | cut -f5`
year=`date +"%Y"`
month=`date +"%m"`
day=`date +"%d"`
sourcesqlfile='/tmp/'$year$month$day'.sql'
destinationsqlfile=$year$month$day'.sql.gz'
passw=

/usr/bin/mysqldump -u root -p$passw hkjc -r $sourcesqlfile > /dev/null 2>&1

if [ -f $sourcesqlfile ]
then 
	/bin/gzip -c $sourcesqlfile > /tmp/$destinationsqlfile 2>/dev/null
	if [ -f /tmp/$destinationsqlfile ]
	then
		/usr/bin/aws s3 cp /tmp/$destinationsqlfile s3://hkjc-rw-prod-log/cms/database_dump/year=$year/month=$month/day=$day/$destinationsqlfile --region ap-northeast-1 > /dev/null 2>&1 
		rm -f /tmp/$destinationsqlfile > /dev/null 2>&1
	fi
	rm -f $sourcesqlfile > /dev/null 2>&1
fi

